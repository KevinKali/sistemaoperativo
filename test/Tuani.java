//import Login.FileLogin;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//public class Tuani {
//    private static void copyFile(File source, File target) throws IOException {
//        if (!source.exists()) {
//            try (
//                InputStream in = new FileInputStream(source);
//                OutputStream out = new FileOutputStream(target)) {
//
//                byte[] buffer = new byte[1024];
//                int length;
//                while ((length = in.read(buffer)) > 0) {
//                    out.write(buffer, 0, length);
//                }
//            }
//        } else{
//            System.out.println("adwdas");
//        }
//        
//    }
//
//    public static void main(String args[]) {
//        try {
//            Tuani.copyFile(new File("C:\\practicaSO\\a.jpg"),new File(FileLogin.routeTrash,"tuani.jpg"));
//        } catch (IOException ex) {
//            Logger.getLogger(Tuani.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
////    public static void main(String[] args) {
////        File createFile = new File(FileLogin.routeTrash,"Images");
////        createFile.mkdir();
////        
////        String pathSource = FileLogin.routeImages;
////        String pathTarget = FileLogin.routeTrash+File.separator+"Documentos";
////
////        //(new File(FileLogin.routeImages)).delete();
////        
//////        try {
//////            CopiarDirectorio.copy(pathSource, pathTarget);
//////        } catch (IOException ex) {
//////            ex.getMessage();
//////        }
////        
//////        File ficheroCopiar = new File(FileLogin.routePrincipal+File.separator+"a.jpg");
//////        File ficheroDestino = new File(FileLogin.routeTrash);
//////            try {
//////                //Files.copy(Paths.get(ficheroCopiar.getAbsolutePath()), Paths.get(ficheroDestino.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
//////                Files.copy(Paths.get(FileLogin.routePrincipal+File.separator+"a.jpg"), Paths.get(FileLogin.routeTrash), StandardCopyOption.REPLACE_EXISTING);
//////            } catch (IOException ex) {
//////                
//////            }
////    }
//}
//
//class CopiarDirectorio {
//
//    public static void copy(File sourceLocation, File targetLocation) throws IOException {
//        if (sourceLocation.isDirectory()) {
//            copyDirectory(sourceLocation, targetLocation);
//        } else {
//            copyFile(sourceLocation, targetLocation);
//        }
//    }
//
//     public static void copy(String sSourceLocation, String stargetLocation) throws IOException {         
//         File sourceLocation = new File(sSourceLocation);
//         File targetLocation = new File(stargetLocation);
//
//        if (sourceLocation.isDirectory()) { //Es directorio.
//            copyDirectory(sourceLocation, targetLocation);
//        } else { //Es archivo.
//            copyFile(sourceLocation, targetLocation);
//        }
//    }
//
//    private static void copyDirectory(File source, File target) throws IOException {
//        if (!target.exists()) {
//            //No existe directorio destino, lo crea.
//            target.mkdir();
//        }
//        for (String f : source.list()) {
//            //Copia archivos de directorio fuente a destino.
//            copy(new File(source, f), new File(target, f));
//        }
//    }
//
//    private static void copyFile(File source, File target) throws IOException {
//        try (
//            InputStream in = new FileInputStream(source);
//            OutputStream out = new FileOutputStream(target)) {
//            byte[] buffer = new byte[1024];
//            int length;
//            while ((length = in.read(buffer)) > 0) {
//                out.write(buffer, 0, length);
//            }
//        }
//    }
//
//}

package org.xulescode.javaio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Tuani {
    public boolean copyFile(String fromFile, String toFile) {
        File origin = new File(fromFile);
        File destination = new File(toFile);
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    
    public static void main(String args[]) {
        
        String fromFile = "C:/practicaSO/a.jpg";
        String toFile = "C:/practicaSO/Papelera/a.jpg";
        
        File origin = new File(fromFile);
        File destination = new File(toFile);
        
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
        }

    }
}