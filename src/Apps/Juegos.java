
package Apps;

import DosCaras.Parejas;
import static Escritorio.Escritorio_Principal.escritorio;
import ahorcado.ahorcado.Ahorcado1;
import gato.JugadoresX0;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Juegos extends javax.swing.JInternalFrame 
{

    public Juegos() {
        initComponents();
        ImageIcon nav = new ImageIcon(getClass().getResource("/Images/ahor.png"));
        Image conver = nav.getImage();
        Image tam = conver.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
        ImageIcon na=new ImageIcon(tam);
        btnAhorcado.setIcon(na);
        
        ImageIcon block = new ImageIcon(getClass().getResource("/Images/par.png"));
        Image conve = block.getImage();
        Image tama = conve.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
        ImageIcon bl=new ImageIcon(tama);
        btnPares.setIcon(bl);
        
        ImageIcon cal = new ImageIcon(getClass().getResource("/Images/gat.png"));
        Image conv = cal.getImage();
        Image tamanio = conv.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
        ImageIcon ca=new ImageIcon(tamanio);
        btnx0.setIcon(ca);
        
        ImageIcon juegos = new ImageIcon(getClass().getResource("/Images/salir.png"));
        Image convertir = juegos.getImage();
        Image longi = convertir.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
        ImageIcon jue=new ImageIcon(longi);
        btnSalir.setIcon(jue);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        panelCurves1 = new org.edisoncor.gui.panel.PanelCurves();
        jLabel1 = new javax.swing.JLabel();
        btnAhorcado = new javax.swing.JButton();
        btnPares = new javax.swing.JButton();
        btnx0 = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("                                                                                                                                    JUEGOS");
        setToolTipText("");
        setPreferredSize(new java.awt.Dimension(900, 440));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/fondo1.jpg"))); // NOI18N
        panelImage1.setPreferredSize(new java.awt.Dimension(685, 414));
        panelImage1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panelCurves1.setBackground(new java.awt.Color(0, 0, 0));
        panelCurves1.setPreferredSize(new java.awt.Dimension(800, 440));

        javax.swing.GroupLayout panelCurves1Layout = new javax.swing.GroupLayout(panelCurves1);
        panelCurves1.setLayout(panelCurves1Layout);
        panelCurves1Layout.setHorizontalGroup(
            panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelCurves1Layout.setVerticalGroup(
            panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        panelImage1.add(panelCurves1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 886, 120));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("¡¡¡ BIENVENIDO A LOS JUEGOS !!!");
        panelImage1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(151, 39, -1, -1));

        btnAhorcado.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        btnAhorcado.setText("Ahorcado");
        btnAhorcado.setBorder(null);
        btnAhorcado.setBorderPainted(false);
        btnAhorcado.setContentAreaFilled(false);
        btnAhorcado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAhorcado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAhorcado.setIconTextGap(-3);
        btnAhorcado.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnAhorcado.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAhorcado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAhorcadoActionPerformed(evt);
            }
        });
        panelImage1.add(btnAhorcado, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 170, 180));

        btnPares.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        btnPares.setText("Parejas");
        btnPares.setBorder(null);
        btnPares.setBorderPainted(false);
        btnPares.setContentAreaFilled(false);
        btnPares.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPares.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPares.setIconTextGap(-3);
        btnPares.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnPares.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParesActionPerformed(evt);
            }
        });
        panelImage1.add(btnPares, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 120, 170, 180));

        btnx0.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        btnx0.setText("3 En Linea - X0");
        btnx0.setBorder(null);
        btnx0.setBorderPainted(false);
        btnx0.setContentAreaFilled(false);
        btnx0.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnx0.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnx0.setIconTextGap(-3);
        btnx0.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnx0.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnx0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnx0ActionPerformed(evt);
            }
        });
        panelImage1.add(btnx0, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 120, 170, 180));

        btnSalir.setFont(new java.awt.Font("Arial", 1, 15)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.setBorder(null);
        btnSalir.setBorderPainted(false);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setIconTextGap(-3);
        btnSalir.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        panelImage1.add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 120, 170, 180));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, 886, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnParesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParesActionPerformed
        Parejas pare=new Parejas();
        escritorio.add(pare);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = pare.getSize();
        pare.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        pare.show();
        
    }//GEN-LAST:event_btnParesActionPerformed

    private void btnAhorcadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAhorcadoActionPerformed
        Ahorcado1 ah=new Ahorcado1();
        escritorio.add(ah);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = ah.getSize();
        ah.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        ah.show();
        
    }//GEN-LAST:event_btnAhorcadoActionPerformed

    private void btnx0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnx0ActionPerformed
        JugadoresX0 x0=new JugadoresX0();
        escritorio.add(x0);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = x0.getSize();
        x0.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        x0.show();
        this.dispose();
    }//GEN-LAST:event_btnx0ActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAhorcado;
    private javax.swing.JButton btnPares;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnx0;
    private javax.swing.JLabel jLabel1;
    private org.edisoncor.gui.panel.PanelCurves panelCurves1;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    // End of variables declaration//GEN-END:variables
}
