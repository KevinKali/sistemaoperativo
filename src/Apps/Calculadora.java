package Apps;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Calculadora extends javax.swing.JInternalFrame 
{
    private boolean punto=true;
    String valor1,valor2,contenido;
    String signo;
    Double resultado;
    
    public Calculadora() 
    {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelC = new javax.swing.JPanel();
        txtC = new javax.swing.JTextField();
        btnEntre = new javax.swing.JButton();
        btnEx = new javax.swing.JButton();
        btnIgual = new javax.swing.JButton();
        btnPor = new javax.swing.JButton();
        btn1x = new javax.swing.JButton();
        btn8 = new javax.swing.JButton();
        btn5 = new javax.swing.JButton();
        btn2 = new javax.swing.JButton();
        btn0 = new javax.swing.JButton();
        btnx2 = new javax.swing.JButton();
        btn9 = new javax.swing.JButton();
        btn6 = new javax.swing.JButton();
        btn3 = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        btnC = new javax.swing.JButton();
        raiz = new javax.swing.JButton();
        btn7 = new javax.swing.JButton();
        btn4 = new javax.swing.JButton();
        btn1 = new javax.swing.JButton();
        btnMasMenos = new javax.swing.JButton();
        btnxy = new javax.swing.JButton();
        btnMas = new javax.swing.JButton();
        btnMenos = new javax.swing.JButton();
        btnPunto = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("                                                                 Calculadora");

        panelC.setBackground(new java.awt.Color(204, 255, 204));
        panelC.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtC.setEditable(false);
        txtC.setBackground(new java.awt.Color(255, 255, 255));
        txtC.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        txtC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCKeyTyped(evt);
            }
        });
        panelC.add(txtC, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 460, 61));

        btnEntre.setBackground(new java.awt.Color(153, 255, 153));
        btnEntre.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnEntre.setText("/");
        btnEntre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEntreActionPerformed(evt);
            }
        });
        panelC.add(btnEntre, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 270, 60, 50));

        btnEx.setBackground(new java.awt.Color(153, 255, 153));
        btnEx.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnEx.setText("e^x");
        btnEx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExActionPerformed(evt);
            }
        });
        panelC.add(btnEx, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 70, 50));

        btnIgual.setBackground(new java.awt.Color(153, 255, 153));
        btnIgual.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnIgual.setText("=");
        btnIgual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIgualActionPerformed(evt);
            }
        });
        panelC.add(btnIgual, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 330, 60, 50));

        btnPor.setBackground(new java.awt.Color(153, 255, 153));
        btnPor.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnPor.setText("*");
        btnPor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPorActionPerformed(evt);
            }
        });
        panelC.add(btnPor, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 210, 60, 50));

        btn1x.setBackground(new java.awt.Color(153, 255, 153));
        btn1x.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn1x.setText("1/x");
        btn1x.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1xActionPerformed(evt);
            }
        });
        panelC.add(btn1x, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 70, 50));

        btn8.setBackground(new java.awt.Color(153, 255, 153));
        btn8.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn8.setText("8");
        btn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8ActionPerformed(evt);
            }
        });
        panelC.add(btn8, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 150, 60, 50));

        btn5.setBackground(new java.awt.Color(153, 255, 153));
        btn5.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn5.setText("5");
        btn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5ActionPerformed(evt);
            }
        });
        panelC.add(btn5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 210, 60, 50));

        btn2.setBackground(new java.awt.Color(153, 255, 153));
        btn2.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn2.setText("2");
        btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2ActionPerformed(evt);
            }
        });
        panelC.add(btn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 60, 50));

        btn0.setBackground(new java.awt.Color(153, 255, 153));
        btn0.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn0.setText("0");
        btn0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn0ActionPerformed(evt);
            }
        });
        panelC.add(btn0, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 330, 140, 50));

        btnx2.setBackground(new java.awt.Color(153, 255, 153));
        btnx2.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnx2.setText("x^2");
        btnx2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnx2ActionPerformed(evt);
            }
        });
        panelC.add(btnx2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 90, 70, 50));

        btn9.setBackground(new java.awt.Color(153, 255, 153));
        btn9.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn9.setText("9");
        btn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn9ActionPerformed(evt);
            }
        });
        panelC.add(btn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 150, 60, 50));

        btn6.setBackground(new java.awt.Color(153, 255, 153));
        btn6.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn6.setText("6");
        btn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6ActionPerformed(evt);
            }
        });
        panelC.add(btn6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 60, 50));

        btn3.setBackground(new java.awt.Color(153, 255, 153));
        btn3.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn3.setText("3");
        btn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3ActionPerformed(evt);
            }
        });
        panelC.add(btn3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 270, 60, 50));

        btnBorrar.setBackground(new java.awt.Color(153, 255, 153));
        btnBorrar.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnBorrar.setText("<--");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        panelC.add(btnBorrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, 140, 50));

        btnC.setBackground(new java.awt.Color(153, 255, 153));
        btnC.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnC.setText("C");
        btnC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCActionPerformed(evt);
            }
        });
        panelC.add(btnC, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 210, 60, 170));

        raiz.setBackground(new java.awt.Color(153, 255, 153));
        raiz.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        raiz.setText("Raiz");
        raiz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                raizActionPerformed(evt);
            }
        });
        panelC.add(raiz, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 90, 50));

        btn7.setBackground(new java.awt.Color(153, 255, 153));
        btn7.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn7.setText("7");
        btn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7ActionPerformed(evt);
            }
        });
        panelC.add(btn7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 60, 50));

        btn4.setBackground(new java.awt.Color(153, 255, 153));
        btn4.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn4.setText("4");
        btn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4ActionPerformed(evt);
            }
        });
        panelC.add(btn4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 60, 50));

        btn1.setBackground(new java.awt.Color(153, 255, 153));
        btn1.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btn1.setText("1");
        btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1ActionPerformed(evt);
            }
        });
        panelC.add(btn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 60, 50));

        btnMasMenos.setBackground(new java.awt.Color(153, 255, 153));
        btnMasMenos.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnMasMenos.setText("+ -");
        btnMasMenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMasMenosActionPerformed(evt);
            }
        });
        panelC.add(btnMasMenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, 60, 50));

        btnxy.setBackground(new java.awt.Color(153, 255, 153));
        btnxy.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnxy.setText("x^y");
        btnxy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnxyActionPerformed(evt);
            }
        });
        panelC.add(btnxy, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 90, 70, 50));

        btnMas.setBackground(new java.awt.Color(153, 255, 153));
        btnMas.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnMas.setText("+");
        btnMas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMasActionPerformed(evt);
            }
        });
        panelC.add(btnMas, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 150, 60, 110));

        btnMenos.setBackground(new java.awt.Color(153, 255, 153));
        btnMenos.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnMenos.setText("-");
        btnMenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenosActionPerformed(evt);
            }
        });
        panelC.add(btnMenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 270, 60, 50));

        btnPunto.setBackground(new java.awt.Color(153, 255, 153));
        btnPunto.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        btnPunto.setText(".");
        btnPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPuntoActionPerformed(evt);
            }
        });
        panelC.add(btnPunto, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 330, 60, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelC, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelC, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEntreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEntreActionPerformed
        if(!txtC.getText().equals("")){
            valor1=txtC.getText();
            signo="/";
            txtC.setText("");
        }
    }//GEN-LAST:event_btnEntreActionPerformed

    private void btnExActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0){
            resultado=Math.exp(Double.parseDouble(contenido));
            txtC.setText(resultado.toString());
        }
        else
            if(contenido.length()==0){
                resultado=Math.exp(1);
                txtC.setText(resultado.toString());
            }
        
    }//GEN-LAST:event_btnExActionPerformed

    private void btnIgualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIgualActionPerformed
        String resultadototal;
        valor2=txtC.getText();
        if(!valor2.equals("")){
            resultadototal=operaciones(valor1,valor2,signo);
            txtC.setText(resultadototal);
        }
    }//GEN-LAST:event_btnIgualActionPerformed

    private void btnPorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPorActionPerformed
        if(!txtC.getText().equals("")){
            valor1=txtC.getText();
            signo="*";
            txtC.setText("");
        }
    }//GEN-LAST:event_btnPorActionPerformed

    private void btn1xActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1xActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0){
            resultado=1/(Double.parseDouble(contenido));
            txtC.setText(resultado.toString());
        }
        else
            if(contenido.length()==0){
                resultado=1/1.0;
                txtC.setText(resultado.toString());
            }
    }//GEN-LAST:event_btn1xActionPerformed

    private void btn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8ActionPerformed
        txtC.setText(txtC.getText()+"8");
    }//GEN-LAST:event_btn8ActionPerformed

    private void btn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5ActionPerformed
        txtC.setText(txtC.getText()+"5");
    }//GEN-LAST:event_btn5ActionPerformed

    private void btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2ActionPerformed
        txtC.setText(txtC.getText()+"2");
    }//GEN-LAST:event_btn2ActionPerformed

    private void btn0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn0ActionPerformed
        txtC.setText(txtC.getText()+"0");
    }//GEN-LAST:event_btn0ActionPerformed

    private void btnx2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnx2ActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0){
            resultado=Math.pow(Double.parseDouble(contenido), 2);
            txtC.setText(resultado.toString());
        }
        else
            if(contenido.length()==0){
                resultado=Math.pow(1,2);;
                txtC.setText(resultado.toString());
            }
    }//GEN-LAST:event_btnx2ActionPerformed

    private void btn9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn9ActionPerformed
        txtC.setText(txtC.getText()+"9");
    }//GEN-LAST:event_btn9ActionPerformed

    private void btn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6ActionPerformed
        txtC.setText(txtC.getText()+"6");
    }//GEN-LAST:event_btn6ActionPerformed

    private void btn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3ActionPerformed
        txtC.setText(txtC.getText()+"3");
    }//GEN-LAST:event_btn3ActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0){
            contenido=contenido.substring(0,contenido.length()-1);;
            txtC.setText(contenido);
        }
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCActionPerformed
        txtC.setText("");
    }//GEN-LAST:event_btnCActionPerformed

    private void raizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_raizActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0){
            resultado=Math.sqrt(Double.parseDouble(contenido));
            txtC.setText(resultado.toString());
        }
    }//GEN-LAST:event_raizActionPerformed

    private void btn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7ActionPerformed
        txtC.setText(txtC.getText()+"7");
    }//GEN-LAST:event_btn7ActionPerformed

    private void btn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4ActionPerformed
        txtC.setText(txtC.getText()+"4");
    }//GEN-LAST:event_btn4ActionPerformed

    private void btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1ActionPerformed
        txtC.setText(txtC.getText()+"1");
    }//GEN-LAST:event_btn1ActionPerformed

    public static String operaciones(String valor1, String valor2, String signo)
    {
        Double resultadocalc = 0.0;
        String respuesta;
        
        if(signo.equals("+"))
        {
            resultadocalc=Double.parseDouble(valor1)+Double.parseDouble(valor2);
        }
        else
            if(signo.equals("-"))
            {
                resultadocalc=Double.parseDouble(valor1)-Double.parseDouble(valor2);
            }
        switch(signo)
        {
            case "+": resultadocalc=Double.parseDouble(valor1)+Double.parseDouble(valor2);
                      break;
                      
            case "-": resultadocalc=Double.parseDouble(valor1)-Double.parseDouble(valor2);
                      break;
                      
            case "*": resultadocalc=Double.parseDouble(valor1)*Double.parseDouble(valor2);
                      break;
                    
            case "/": resultadocalc=Double.parseDouble(valor1)/Double.parseDouble(valor2);
                      break;
                      
            case "x^y": resultadocalc=Math.pow(Double.parseDouble(valor1), Double.parseDouble(valor2));
                        break;
        }
        respuesta=resultadocalc.toString();
        
        return respuesta;
    }
    
    private void btnMasMenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMasMenosActionPerformed
        contenido=txtC.getText();
        if(contenido.length()>0)
        {
            resultado=(-1)*Double.parseDouble(contenido);
            txtC.setText(resultado.toString());
        }
    }//GEN-LAST:event_btnMasMenosActionPerformed

    private void btnxyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnxyActionPerformed
        if(!txtC.getText().equals("")){
            valor1=txtC.getText();
            signo="x^y";
            txtC.setText("");
        }
    }//GEN-LAST:event_btnxyActionPerformed

    private void btnMasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMasActionPerformed
        if(!txtC.getText().equals("")){
            valor1=txtC.getText();
            signo="+";
            txtC.setText("");
        }
    }//GEN-LAST:event_btnMasActionPerformed

    private void btnMenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenosActionPerformed
        if(!txtC.getText().equals("")){
            valor1=txtC.getText();
            signo="-";
            txtC.setText("");
        }
    }//GEN-LAST:event_btnMenosActionPerformed

    private void btnPuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPuntoActionPerformed
        contenido=txtC.getText();
        if(contenido.length()<0)
        {
            txtC.setText("0.");
        }
        else
            if(txtC.getText().contains(".")){    
            }
            else
            {
                txtC.setText(txtC.getText()+".");
                punto = false;
            }  
    }//GEN-LAST:event_btnPuntoActionPerformed

    private void txtCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCKeyTyped

    }//GEN-LAST:event_txtCKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn0;
    private javax.swing.JButton btn1;
    private javax.swing.JButton btn1x;
    private javax.swing.JButton btn2;
    private javax.swing.JButton btn3;
    private javax.swing.JButton btn4;
    private javax.swing.JButton btn5;
    private javax.swing.JButton btn6;
    private javax.swing.JButton btn7;
    private javax.swing.JButton btn8;
    private javax.swing.JButton btn9;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnC;
    private javax.swing.JButton btnEntre;
    private javax.swing.JButton btnEx;
    private javax.swing.JButton btnIgual;
    private javax.swing.JButton btnMas;
    private javax.swing.JButton btnMasMenos;
    private javax.swing.JButton btnMenos;
    private javax.swing.JButton btnPor;
    private javax.swing.JButton btnPunto;
    private javax.swing.JButton btnx2;
    private javax.swing.JButton btnxy;
    private javax.swing.JPanel panelC;
    private javax.swing.JButton raiz;
    private javax.swing.JTextField txtC;
    // End of variables declaration//GEN-END:variables
}
