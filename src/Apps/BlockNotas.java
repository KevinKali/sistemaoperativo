package Apps;

import Escritorio.Escritorio_Principal;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;


public class BlockNotas extends javax.swing.JInternalFrame {
        String nombreArchivo="";
        String[] apilar=new String[3];
        int DesH;
        int contar;

    public BlockNotas(int n) 
    {
        DesH=0;
        contar=n;
        initComponents();
         {
        addInternalFrameListener(new InternalFrameAdapter(){
            public void internalFrameClosed(InternalFrameEvent e) {
                
            }
        });
    } 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        filechooser = new javax.swing.JFileChooser();
        TPanel = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtArea = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        nuevoA = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        guardarA = new javax.swing.JMenuItem();
        abrirA = new javax.swing.JMenuItem();
        guardarAC = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        deshacer = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        borrarT = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("                                                                                 Block de Notas");
        setFrameIcon(null);
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameDeiconified(evt);
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        txtArea.setColumns(20);
        txtArea.setRows(5);
        txtArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAreaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtArea);

        TPanel.addTab("", jScrollPane1);

        jMenu1.setText("Archivo");

        nuevoA.setText("Nuevo Archivo");
        nuevoA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoAActionPerformed(evt);
            }
        });
        jMenu1.add(nuevoA);

        jSeparator1.setBackground(new java.awt.Color(219, 215, 210));
        jMenu1.add(jSeparator1);

        guardarA.setText("Guardar Archivo");
        guardarA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarAActionPerformed(evt);
            }
        });
        jMenu1.add(guardarA);

        abrirA.setText("Abrir Archivo");
        abrirA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abrirAActionPerformed(evt);
            }
        });
        jMenu1.add(abrirA);

        guardarAC.setText("Guardar Como");
        guardarAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarACActionPerformed(evt);
            }
        });
        jMenu1.add(guardarAC);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");

        deshacer.setText("Deshacer");
        deshacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deshacerActionPerformed(evt);
            }
        });
        jMenu2.add(deshacer);
        jMenu2.add(jSeparator3);

        borrarT.setText("Borrar Todo");
        borrarT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrarTActionPerformed(evt);
            }
        });
        jMenu2.add(borrarT);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 612, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(TPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(TPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nuevoAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevoAActionPerformed
        if(txtArea.getText().isEmpty()== false){
            JDialog.setDefaultLookAndFeelDecorated(true);
            int response = JOptionPane.showConfirmDialog(null, "Do you want to save changes ?","Confirmation",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

            if(response == JOptionPane.NO_OPTION){
                txtArea.setText("");
            }
            else if(response == JOptionPane.YES_OPTION){
                guardarAActionPerformed(evt);
                nombreArchivo="";
                txtArea.setText("");
            }
        }  
    }//GEN-LAST:event_nuevoAActionPerformed

    private void guardarAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarAActionPerformed

        if(txtArea.getText().isEmpty()== false){
            System.out.println(nombreArchivo);
            JDialog.setDefaultLookAndFeelDecorated(true);

            if(nombreArchivo!="")
            {
                TPanel.setTitleAt(0, nombreArchivo.substring(nombreArchivo.lastIndexOf('/')+1));
                try
                {
                    PrintWriter writer=new PrintWriter(nombreArchivo,"UTF-8");
                    writer.print(txtArea.getText());
                    writer.close();
                }
                catch (Exception e) {
                }

            }
            else
            {
                try
                {
                    filechooser.setDialogTitle("Save file");
                    int returnVal = filechooser.showSaveDialog(this);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = filechooser.getSelectedFile();
                        try {

                            nombreArchivo=file.getAbsolutePath();
                            TPanel.setTitleAt(0, file.getName());
                            PrintWriter writer=new PrintWriter(nombreArchivo,"UTF-8");
                            writer.print(txtArea.getText());
                            writer.close();

                        } catch (IOException ex) {
                            System.out.println("problem accessing file"+file.getAbsolutePath());
                        }
                    }

                }
                catch (Exception e) {
                }
            }

        }

    }//GEN-LAST:event_guardarAActionPerformed

    private void abrirAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_abrirAActionPerformed
        filechooser.setDialogTitle("Open a file");
        int returnVal = filechooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = filechooser.getSelectedFile();
            try {
                txtArea.read( new FileReader( file.getAbsolutePath() ), null );
                nombreArchivo=file.getAbsolutePath();
                TPanel.setTitleAt(0, file.getName());;
            } catch (IOException ex) {
                System.out.println("problem accessing file"+file.getAbsolutePath());
            }
        }

    }//GEN-LAST:event_abrirAActionPerformed

    public void JInternalFrameClosed(InternalFrameEvent e){
        //System.out.println("closed");
    }
    private void guardarACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarACActionPerformed
        if(txtArea.getText().isEmpty()== false){
            System.out.println(nombreArchivo);
            JDialog.setDefaultLookAndFeelDecorated(true);
            try
            {
                filechooser.setDialogTitle("Save As");
                int returnVal = filechooser.showSaveDialog(this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = filechooser.getSelectedFile();
                    try {

                        nombreArchivo=file.getAbsolutePath();
                        PrintWriter writer=new PrintWriter(nombreArchivo,"UTF-8");
                        writer.print(txtArea.getText());
                        writer.close();

                    } catch (IOException ex) {
                        System.out.println("problem accessing file"+file.getAbsolutePath());
                    }
                }

            }
            catch (Exception e) {
            }
        }

    }//GEN-LAST:event_guardarACActionPerformed
    public void appendtota(String s) throws IOException{
            try {
                txtArea.read(new FileReader(s),null);
                TPanel.setTitleAt(0,s.substring(s.lastIndexOf('/')+1));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BlockNotas.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    private void deshacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deshacerActionPerformed
        DesH--;
        txtArea.setText(apilar[DesH]);
    }//GEN-LAST:event_deshacerActionPerformed

    private void txtAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAreaKeyPressed
        if(DesH==3)
            DesH=0;
        apilar[DesH]=txtArea.getText();
        DesH++;
        
    }//GEN-LAST:event_txtAreaKeyPressed

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed
        Escritorio_Principal.block[contar]=null;
    }//GEN-LAST:event_formInternalFrameClosed

    private void formInternalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameDeiconified
        for(int j=0;j<10;j++)
        {

                if(Escritorio_Principal.block[j]!=null&&j!=contar){
                    try {
                            Escritorio_Principal.block[j].setIcon(true);
                    } catch (PropertyVetoException ex) {
                        Logger.getLogger(Escritorio_Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        
    }//GEN-LAST:event_formInternalFrameDeiconified

    private void borrarTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrarTActionPerformed
        txtArea.setText("");
    }//GEN-LAST:event_borrarTActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTabbedPane TPanel;
    private javax.swing.JMenuItem abrirA;
    private javax.swing.JMenuItem borrarT;
    private javax.swing.JMenuItem deshacer;
    private javax.swing.JFileChooser filechooser;
    private javax.swing.JMenuItem guardarA;
    private javax.swing.JMenuItem guardarAC;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JMenuItem nuevoA;
    public static javax.swing.JTextArea txtArea;
    // End of variables declaration//GEN-END:variables
}
