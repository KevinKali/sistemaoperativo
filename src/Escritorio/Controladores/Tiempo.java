
package Escritorio.Controladores;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Tiempo extends Thread{
    public final String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    public final String diasW[] = {"dom", "lun", "mar", "mie", "jue", "vie", "sab"};
    
    javax.swing.JLabel time, date;
    String hora,minutos,seg;
    String diaW, diaM, mes;
    Calendar calendario;
    int i;
    
    public Tiempo(javax.swing.JLabel time, javax.swing.JLabel date){
        calendario = new GregorianCalendar();
        (new Thread(this)).start();
        
        this.time = time;
        this.date = date;
        
        inicializaTiempo();
    }
    
    @Override
    public void run() {
        
        while(true){
            calcule();
            time.setText(hora+":"+minutos+":"+seg);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {}
        }
    }

    private void calcule() {
        calendario = new GregorianCalendar();
        
        if(Calendar.HOUR == 0 && Calendar.MINUTE == 0){
            inicializaTiempo();
        }
            
        i=calendario.get(Calendar.HOUR_OF_DAY);
        hora = i>9? ""+i : "0"+i;

        i=calendario.get(Calendar.MINUTE);
        minutos = i>9 ? ""+i : "0"+i;
        
        i=calendario.get(Calendar.SECOND);
        seg = i>9 ? ""+i : "0"+i;
    }

    private void inicializaTiempo() {
        mes = meses[calendario.get(Calendar.MONTH)];
        diaM =Integer.toString(calendario.get(Calendar.DAY_OF_MONTH));
        diaW = diasW[calendario.get(Calendar.DAY_OF_WEEK)-1];

        date.setText(diaW +", "+ diaM +" de "+mes +", ");
    }
}
