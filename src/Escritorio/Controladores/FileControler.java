
package Escritorio.Controladores;

import Escritorio.Carpeta;
import Escritorio.FileIMG;
import Login.FileLogin;
import java.io.*;
import java.util.ArrayList;
import org.apache.commons.io.FilenameUtils;

public class FileControler{
    static File rutaF;
    static javax.swing.JPanel archivos_panel;
    static javax.swing.JLabel nombreFile;
    static javax.swing.JTextField rutaText;
    static javax.swing.JLabel to;
    static javax.swing.JPanel panel_img;
    public static String rutaEspecifica;
    public static String atrasRuta;
    public static ArrayList<String> rutasBack;
    public static int tipoVista=1;
    
    public FileControler(javax.swing.JPanel archivos_panel,javax.swing.JLabel nombreFile, javax.swing.JTextField rutaText){
        FileControler.archivos_panel = archivos_panel;
        FileControler.nombreFile = nombreFile;
        FileControler.rutaText = rutaText;
        
        rutasBack=new ArrayList();
    }
    
    public static void setFileControler(javax.swing.JPanel archivos_panel,javax.swing.JLabel nombreFile, javax.swing.JTextField rutaText){
        FileControler.archivos_panel = archivos_panel;
        FileControler.nombreFile = nombreFile;
        FileControler.rutaText = rutaText;
        
        rutasBack=new ArrayList();
    }
      
    public static String getTipoFile(String n){
        return FilenameUtils.getExtension(n);
    }

    public static void siguiente(String t){
        rutasBack.add(t);
        if(tipoVista==1)
            mostrar(rutasBack.get(rutasBack.size()-1));
        else
            moverMostrarFile(rutasBack.get(rutasBack.size()-1));
    }

    public static void atras(){
        if(rutasBack.size()>1){
            rutasBack.remove(rutasBack.size()-1);
            if(tipoVista==1)
                mostrar(rutasBack.get(rutasBack.size()-1));
            else
                moverMostrarFile(rutasBack.get(rutasBack.size()-1));
        }
    }
      
      public static void mostrar(String t){
        tipoVista=1;
        rutaEspecifica=t;
        
        rutaF = new File(t);
        String[] nombres_archivos=rutaF.list();
        rutaText.setText(t);
        
        archivos_panel.removeAll();
        
        for (String nombres_archivo : nombres_archivos) {
            if (FileControler.getTipoFile(nombres_archivo).equals("jpg") || FileControler.getTipoFile(nombres_archivo).equals("png")) {
                archivos_panel.add(new Carpeta(nombres_archivo, "/Images/Icons/img_Icon.jpg", nombreFile, "image"));
            } else if (FileControler.getTipoFile(nombres_archivo).equals("txt")) {
                archivos_panel.add(new Carpeta(nombres_archivo, "/Images/Icons/txt_Icon.png", nombreFile, "file"));
            } else {
                if(!nombres_archivo.equals("Papelera"))
                    archivos_panel.add(new Carpeta(nombres_archivo, "/Images/Icons/folder.png", nombreFile, "folder"));
            }
        }
        archivos_panel.repaint();
        archivos_panel.revalidate();
    }
    
      
    public static void asignaPanel(javax.swing.JPanel panel){
        panel_img = panel;
    }
    
    public static void siguienteIMG(String t){
        rutasBack.add(t);
        mostrarImages(rutasBack.get(rutasBack.size()-1));
    }
    
    public static void atrasIMG(){
        if(rutasBack.size()>1){
            rutasBack.remove(rutasBack.size()-1);
                mostrarImages(rutasBack.get(rutasBack.size()-1));
        }
    }
      
    public static void mostrarImages(String t){
        tipoVista=1;
        rutaEspecifica=t;
        
        rutaF = new File(t);
        String[] nombres_archivos=rutaF.list();
        rutaText.setText(t);
        
        archivos_panel.removeAll();
        
        for (String nombres_archivo : nombres_archivos) {
            if(FileControler.getTipoFile(nombres_archivo).equals(""))
                archivos_panel.add(new FileIMG(nombres_archivo, "/Images/Icons/carpeta.jpg", nombreFile, "folder",panel_img));
            else
                archivos_panel.add(new FileIMG(nombres_archivo, "/Images/Icons/img_Icon.jpg", nombreFile, "image",panel_img));
        }
        archivos_panel.repaint();
        archivos_panel.revalidate();
    }
      
      
    public static void setTo_(javax.swing.JLabel toD){
        to=toD;
    }
    
    public static void moverMostrarFile(String t){
        tipoVista=0;
        rutaEspecifica=t;
        
        rutaF = new File(t);
        String[] nombres_archivos=rutaF.list();
        rutaText.setText(t);
        
        archivos_panel.removeAll();
        
        for (String nombres_archivo : nombres_archivos) {
            if(FileControler.getTipoFile(nombres_archivo).equals("")){
                if(!nombres_archivo.equals("Papelera"))
                    archivos_panel.add(new Carpeta(nombres_archivo, "/Images/Icons/folder.png", to, "folder"));
            }
        }
        archivos_panel.repaint();
        archivos_panel.revalidate();
    }
      
    public static void createFolder(String f){
        rutaF = new File(rutaEspecifica+File.separator+f);
        rutaF.mkdir();
        mostrar(rutaEspecifica);
    }
    
    public static void deleteFile(String f){
        
        if(!rutaEspecifica.equals(FileLogin.routeTrash))
            mover(f,FileLogin.routeTrash);
        
        deleteFinal(new File(rutaEspecifica+File.separator+f));
        
        mostrar(rutaEspecifica);
    }
    
    public static void deleteFinal(File fileDel){
        if(fileDel.isDirectory()){
            if(fileDel.list().length == 0)
                fileDel.delete();
            else{
               
               for (String temp : fileDel.list()) {
                   File fileDelete = new File(fileDel, temp);
                   
                   deleteFinal(fileDelete);
               }
               if(fileDel.list().length==0)
                   fileDel.delete();
            }
        }else{
            fileDel.delete();           
        }
    }
    
    public static void mover(String f,String destino){
        if(getTipoFile(rutaEspecifica+File.separator+f).equals("")){
            File createFile = new File(destino,f);
            createFile.mkdirs();
            
            String pathSource = rutaEspecifica+File.separator+f;
            String pathTarget = destino+File.separator+f;
            
            try {
                CopiarDirectorio.copy(pathSource, pathTarget);
            } catch (IOException ex) {}
        }else{
            moverFile(rutaEspecifica+File.separator+f, destino+File.separator+f);
        }
    }
    
    private static void moverFile(String fromFile, String toFile){
        File origin = new File(fromFile);
        File destination = new File(toFile);
        
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                
            } catch (IOException ioe) {
            }
        } else {}
    }

    public static void moverTo(String archivo, String destino, String nombre){
        if(getTipoFile(archivo).equals("")){
            File createFile = new File(destino,nombre);
            createFile.mkdirs();
            
            String pathSource = archivo;
            String pathTarget = destino+File.separator+nombre;
            
            try {
                CopiarDirectorio.copy(pathSource, pathTarget);
            } catch (IOException ex) {}
        }else{
            moverFile(archivo, destino+File.separator+nombre);
        }
        
        mostrar(rutaEspecifica);
    }
    
    public static void renombrar(String actual, String destino){
        File oldfile = new File(actual);
        File newfile = new File(destino);
        
        if (oldfile.renameTo(newfile)) {
            System.out.println("archivo renombrado");
        } else {
            System.out.println("error");
        }
    }
}
