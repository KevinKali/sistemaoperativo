package Escritorio.Controladores;

public class ControlaHiloTime implements Runnable{

    Tiempo t;
    
    public ControlaHiloTime(javax.swing.JLabel time, javax.swing.JLabel date){
        t= new Tiempo(time,date);
    }
    
    @Override
    public void run() {
        t.start();
    }
    
}
