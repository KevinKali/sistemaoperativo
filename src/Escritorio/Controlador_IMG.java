
package Escritorio;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Controlador_IMG {
    public void setIcono(javax.swing.JButton b, String img, int width, int height){
        b.setText("");
        
        ImageIcon admi;
        admi = new ImageIcon(getClass().getResource(img));
        Image conver = admi.getImage();
        Image tam = conver.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        ImageIcon ad=new ImageIcon(tam);
        b.setIcon(ad);
    }
    
    public void setIconoLable(javax.swing.JLabel b, String img, int width, int height){
        ImageIcon admi;
        admi = new ImageIcon(getClass().getResource(img));
        Image conver = admi.getImage();
        Image tam = conver.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        ImageIcon ad=new ImageIcon(tam);
        b.setIcon(ad);
    }
}
