
package Escritorio;

import Escritorio.Controladores.ControlAccesoSwing;
import Escritorio.Controladores.FileControler;
import Login.FileLogin;
import java.awt.Toolkit;
import java.io.File;

public class TrashGestor extends javax.swing.JInternalFrame {
    String anterior, destino;
    File rutaF;
    int press;
    
    public TrashGestor() {
        press=0;
        initComponents();
        
        ControlAccesoSwing.hacerClick=false;
        this.setPreferredSize(new java.awt.Dimension(500, 500));
        
        FileControler.setFileControler(archivos_panel, nombreFile, rutaText);
        FileControler.mostrar(FileLogin.routeTrash);
        nombreFile.setText("Papelera");

        this.setPreferredSize(new java.awt.Dimension(500, 500));
        panelTexto.setVisible(false);
        panelBtn.setVisible(false);
    }
    
    private void setBotones(boolean t){
        btn_move.setEnabled(t);
        btn_erase.setEnabled(t);
        backup.setEnabled(t);
    }
        
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        botones = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btn_move = new javax.swing.JButton();
        backup = new javax.swing.JButton();
        btn_erase = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        rutaText = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        nombreFile = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_atras = new javax.swing.JButton();
        panel_Files = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        archivos_panel = new javax.swing.JPanel();
        AceptaCambios = new javax.swing.JPanel();
        trash_panel = new javax.swing.JPanel();
        panelTexto = new javax.swing.JPanel();
        message = new javax.swing.JLabel();
        from = new javax.swing.JLabel();
        panelBtn = new javax.swing.JPanel();
        accept = new javax.swing.JButton();
        cancel = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        documentos = new javax.swing.JLabel();
        escritorio = new javax.swing.JLabel();
        imagenes = new javax.swing.JLabel();
        back_up = new javax.swing.JLabel();
        trash_ = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.BorderLayout());

        botones.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        btn_move.setText("Mover");
        btn_move.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_moveActionPerformed(evt);
            }
        });
        jPanel3.add(btn_move);

        backup.setText("Restaurar");
        backup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backupActionPerformed(evt);
            }
        });
        jPanel3.add(backup);

        btn_erase.setText("Borrar");
        btn_erase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eraseActionPerformed(evt);
            }
        });
        jPanel3.add(btn_erase);

        botones.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        rutaText.setEditable(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rutaText, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rutaText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        botones.add(jPanel8, java.awt.BorderLayout.PAGE_END);

        jPanel2.add(botones, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setPreferredSize(new java.awt.Dimension(100, 17));
        jPanel4.setLayout(new java.awt.GridLayout());

        nombreFile.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        nombreFile.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombreFile.setText("jLabel1");
        jPanel4.add(nombreFile);

        jPanel2.add(jPanel4, java.awt.BorderLayout.EAST);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setPreferredSize(new java.awt.Dimension(120, 0));

        btn_atras.setText("<");
        btn_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_atrasActionPerformed(evt);
            }
        });
        jPanel7.add(btn_atras);

        jPanel2.add(jPanel7, java.awt.BorderLayout.WEST);

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        panel_Files.setBackground(javax.swing.UIManager.getDefaults().getColor("window"));
        panel_Files.setLayout(new java.awt.BorderLayout());

        jPanel5.setLayout(new java.awt.BorderLayout());

        archivos_panel.setBackground(new java.awt.Color(255, 255, 255));
        archivos_panel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("CheckBox.shadow")));
        archivos_panel.setPreferredSize(new java.awt.Dimension(400, 300));
        archivos_panel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEADING));
        jScrollPane1.setViewportView(archivos_panel);

        jPanel5.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        AceptaCambios.setBackground(new java.awt.Color(255, 255, 255));
        AceptaCambios.setLayout(new javax.swing.OverlayLayout(AceptaCambios));

        trash_panel.setLayout(new javax.swing.BoxLayout(trash_panel, javax.swing.BoxLayout.LINE_AXIS));

        panelTexto.setBackground(new java.awt.Color(255, 255, 255));

        message.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        message.setText("Archivo Seleccionado: ");
        panelTexto.add(message);

        from.setText("jLabel1");
        panelTexto.add(from);

        trash_panel.add(panelTexto);

        panelBtn.setBackground(new java.awt.Color(255, 255, 255));

        accept.setText("Aqui?");
        accept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acceptActionPerformed(evt);
            }
        });
        panelBtn.add(accept);

        cancel.setText("Cancelar");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        panelBtn.add(cancel);

        trash_panel.add(panelBtn);

        AceptaCambios.add(trash_panel);

        jPanel5.add(AceptaCambios, java.awt.BorderLayout.PAGE_END);

        panel_Files.add(jPanel5, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledForeground")));
        jPanel6.setPreferredSize(new java.awt.Dimension(120, 0));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 17, 5));

        documentos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        documentos.setText("Documentos");
        documentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                documentosMouseClicked(evt);
            }
        });
        jPanel6.add(documentos);

        escritorio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        escritorio.setText("Escritorio");
        escritorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                escritorioMouseClicked(evt);
            }
        });
        jPanel6.add(escritorio);

        imagenes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        imagenes.setText("Imagenes");
        imagenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagenesMouseClicked(evt);
            }
        });
        jPanel6.add(imagenes);

        back_up.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        back_up.setText("     Backup   ");
        back_up.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                back_upMouseClicked(evt);
            }
        });
        jPanel6.add(back_up);

        trash_.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        trash_.setText("Papelera");
        trash_.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                trash_MouseClicked(evt);
            }
        });
        jPanel6.add(trash_);

        panel_Files.add(jPanel6, java.awt.BorderLayout.WEST);

        jPanel1.add(panel_Files, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_moveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_moveActionPerformed
        if(ControlAccesoSwing.hacerClick==false){
            press=1;
            
            setBotones(false);
            panelBtn.setVisible(true);
            trash_panel.setVisible(true);
            panelBtn.setVisible(true);
            
            if(!nombreFile.getText().equals("Papelera")){
                anterior=FileControler.rutaEspecifica+File.separator+nombreFile.getText();
                FileControler.moverMostrarFile(FileLogin.routeTrash);
                
                message.setText("Archivo Seleccionado.");
                from.setText(nombreFile.getText());
            }else
                Toolkit.getDefaultToolkit().beep();
        }else{
            Toolkit.getDefaultToolkit().beep();
        }
    }//GEN-LAST:event_btn_moveActionPerformed

    private void backupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backupActionPerformed
        if(ControlAccesoSwing.hacerClick==false){
            press=2;

            if(!nombreFile.getText().equals("Papelera")){
                anterior=FileControler.rutaEspecifica+File.separator+nombreFile.getText();
                from.setText(nombreFile.getText());
                
                FileControler.moverTo(anterior, FileLogin.routeBackup, from.getText());
                FileControler.deleteFinal(new File(anterior));
                FileControler.mostrar(FileControler.rutaEspecifica);
                
                message.setText("Archivo Restaurado.");
                String mensaje = FileControler.getTipoFile(anterior).equals("") ? "folder" : FileControler.getTipoFile(anterior);
                
                from.setText(nombreFile.getText()+ ";      Tipo File: "+mensaje);
                
                panelTexto.setVisible(true);
                panelBtn.setVisible(false);
            }else
                Toolkit.getDefaultToolkit().beep();
        }else
            Toolkit.getDefaultToolkit().beep();
    }//GEN-LAST:event_backupActionPerformed

    private void btn_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_atrasActionPerformed
        FileControler.atras();
    }//GEN-LAST:event_btn_atrasActionPerformed

    private void documentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_documentosMouseClicked
        ControlAccesoSwing.hacerClick=true;
        FileControler.siguiente(FileLogin.routeDocument);
    }//GEN-LAST:event_documentosMouseClicked

    private void escritorioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_escritorioMouseClicked
        ControlAccesoSwing.hacerClick=true;
        FileControler.siguiente(FileLogin.routeDesktop);
    }//GEN-LAST:event_escritorioMouseClicked

    private void imagenesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagenesMouseClicked
        ControlAccesoSwing.hacerClick=true;
        FileControler.siguiente(FileLogin.routeImages);
    }//GEN-LAST:event_imagenesMouseClicked

    private void btn_eraseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eraseActionPerformed
        if(ControlAccesoSwing.hacerClick==false){
            press=3;
            
            if(!nombreFile.getText().equals("Papelera")){
                FileControler.deleteFinal(new File(FileControler.rutaEspecifica+File.separator+nombreFile.getText()));
                FileControler.mostrar(FileControler.rutaEspecifica);
                
                panelTexto.setVisible(false);
                panelBtn.setVisible(false);
            }else
                Toolkit.getDefaultToolkit().beep();
        }else
            Toolkit.getDefaultToolkit().beep();
    }//GEN-LAST:event_btn_eraseActionPerformed

    private void trash_MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trash_MouseClicked
        ControlAccesoSwing.hacerClick=false;
        FileControler.siguiente(FileLogin.routeTrash);
        nombreFile.setText("Papelera");
        
    }//GEN-LAST:event_trash_MouseClicked

    private void back_upMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_back_upMouseClicked
        ControlAccesoSwing.hacerClick=true;
        FileControler.siguiente(FileLogin.routeBackup);
    }//GEN-LAST:event_back_upMouseClicked

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        panelTexto.setVisible(false);
        panelBtn.setVisible(false);
        setBotones(true);
    }//GEN-LAST:event_cancelActionPerformed

    private void acceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptActionPerformed
        switch(press){
            case 1: 
                FileControler.moverTo(anterior, destino, from.getText());
                FileControler.deleteFinal(new File(anterior));
                
                setBotones(true);
                panelTexto.setVisible(false);
                panelBtn.setVisible(false);
                break;
            case 2:
                break;
        }
    }//GEN-LAST:event_acceptActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AceptaCambios;
    private javax.swing.JButton accept;
    private javax.swing.JPanel archivos_panel;
    private javax.swing.JLabel back_up;
    private javax.swing.JButton backup;
    private javax.swing.JPanel botones;
    private javax.swing.JButton btn_atras;
    private javax.swing.JButton btn_erase;
    private javax.swing.JButton btn_move;
    private javax.swing.JButton cancel;
    private javax.swing.JLabel documentos;
    private javax.swing.JLabel escritorio;
    private javax.swing.JLabel from;
    private javax.swing.JLabel imagenes;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel message;
    private javax.swing.JLabel nombreFile;
    private javax.swing.JPanel panelBtn;
    private javax.swing.JPanel panelTexto;
    private javax.swing.JPanel panel_Files;
    private javax.swing.JTextField rutaText;
    private javax.swing.JLabel trash_;
    private javax.swing.JPanel trash_panel;
    // End of variables declaration//GEN-END:variables
}
