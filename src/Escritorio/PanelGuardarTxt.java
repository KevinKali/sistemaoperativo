
package Escritorio;

import Escritorio.Controladores.FileControler;
import Login.FileLogin;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JTabbedPane;

public class PanelGuardarTxt extends javax.swing.JInternalFrame {
    String t;
    JTabbedPane TPanel;
    
    public PanelGuardarTxt(String t) {
        initComponents();
        
        
        FileControler.setFileControler(panelC, new javax.swing.JLabel(), rutaTxt);
        FileControler.setTo_(new javax.swing.JLabel());
        FileControler.moverMostrarFile(FileLogin.routeDocument);
    }

    public PanelGuardarTxt(String name, JTabbedPane TPanel) {
        t = name;
        this.TPanel = TPanel;
        
        FileControler.setFileControler(panelC, new javax.swing.JLabel(), rutaTxt);
        FileControler.setTo_(new javax.swing.JLabel());
        FileControler.moverMostrarFile(FileLogin.routePrincipal);
    }
    
    public void crearFile(){
        String nombreArchivo;
        
        File file = new File(rutaTxt.getText());
        
        try {
            nombreArchivo=file.getAbsolutePath();
            TPanel.setTitleAt(0, file.getName());
            
            PrintWriter writer=new PrintWriter(nameFile.getText(),"UTF-8");
            writer.print(t);
            writer.close();

        } catch (IOException ex) {
            System.out.println("problem accessing file"+file.getAbsolutePath());
        }
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        panelC = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        btn_back = new javax.swing.JButton();
        rutaTxt = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        nameFile = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        save = new javax.swing.JButton();
        cancel = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Guardar Cambios");

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel5.setPreferredSize(new java.awt.Dimension(95, 100));

        jButton1.setText("Documentos");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1);

        jPanel1.add(jPanel5, java.awt.BorderLayout.WEST);

        panelC.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panelCLayout = new javax.swing.GroupLayout(panelC);
        panelC.setLayout(panelCLayout);
        panelCLayout.setHorizontalGroup(
            panelCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelCLayout.setVerticalGroup(
            panelCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.add(panelC, java.awt.BorderLayout.CENTER);

        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        btn_back.setText("<");
        jPanel7.add(btn_back);
        jPanel7.add(rutaTxt);

        jPanel1.add(jPanel7, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        jLabel1.setText("Nombre: ");
        jPanel4.add(jLabel1);
        jPanel4.add(nameFile);

        jPanel2.add(jPanel4, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        save.setText("Guardar");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });
        jPanel3.add(save);

        cancel.setText("Cancelar");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        jPanel3.add(cancel);

        jPanel2.add(jPanel3, java.awt.BorderLayout.EAST);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        
        crearFile();
        
    }//GEN-LAST:event_saveActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        
        
        
    }//GEN-LAST:event_cancelActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton cancel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTextField nameFile;
    private javax.swing.JPanel panelC;
    private javax.swing.JTextField rutaTxt;
    private javax.swing.JButton save;
    // End of variables declaration//GEN-END:variables
}
