
package Escritorio;

import Escritorio.Controladores.FileControler;
import Login.FileLogin;
import java.awt.Toolkit;
import java.io.File;

public class GestorFile extends javax.swing.JInternalFrame {
    Controlador_IMG iconosC;
    String cadena;
    File rutaF;
    FileControler fC;
    String anterior;
    String siguiente;
    private static final int widthIcon=32;
    private static final int heightIcon=32;
    
    public GestorFile() {
        initComponents();
        AceptaCambios.setVisible(false);
        
        cadena = FileLogin.routePrincipal;
        nombreFile.setText(FileLogin.routePrincipal);
        setIcons_File();
        
        fC=new FileControler(archivos_panel,nombreFile,rutaText);
        FileControler.siguiente(cadena);
        
        this.setPreferredSize(new java.awt.Dimension(945, 535));
        this.move_panel.setVisible(false);
        this.guardar_Panel.setVisible(false);
    }
    
    private void setIcons_File(){
        iconosC=new Controlador_IMG();
        
        iconosC.setIconoLable(documentos, "/Images/Icons/central.png", widthIcon, heightIcon);
        iconosC.setIconoLable(escritorio, "/Images/Icons/central.png", widthIcon, heightIcon);
        iconosC.setIconoLable(imagenes, "/Images/Icons/central.png", widthIcon, heightIcon);
        iconosC.setIconoLable(backup, "/Images/Icons/central.png", widthIcon, heightIcon);
    }
    
    public void crearCarpeta(){
        if(!nombre.getText().equals("")){
            FileControler.createFolder(nombre.getText());
            AceptaCambios.setVisible(false);
        }else
            Toolkit.getDefaultToolkit().beep();
    }
    
    public void setBotones(boolean b1){
        this.btn_newFile.setEnabled(b1);
        this.btn_erase.setEnabled(b1);
        this.btn_copy.setEnabled(b1);
        this.btn_move.setEnabled(b1);
    }
    
    private void setVistaPanel(boolean v1, boolean v2,boolean v3){
        AceptaCambios.setVisible(v1);
        move_panel.setVisible(v2);
        guardar_Panel.setVisible(v3);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        botones = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btn_newFile = new javax.swing.JButton();
        btn_move = new javax.swing.JButton();
        btn_copy = new javax.swing.JButton();
        btn_erase = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        rutaText = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        nombreFile = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_atras = new javax.swing.JButton();
        panel_Files = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        archivos_panel = new javax.swing.JPanel();
        AceptaCambios = new javax.swing.JPanel();
        move_panel = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        from = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        btn_action = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        guardar_Panel = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        jPanel10 = new javax.swing.JPanel();
        btn_Guardar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        documentos = new javax.swing.JLabel();
        escritorio = new javax.swing.JLabel();
        imagenes = new javax.swing.JLabel();
        backup = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setMinimumSize(new java.awt.Dimension(500, 500));
        setPreferredSize(new java.awt.Dimension(945, 535));

        jPanel1.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.BorderLayout());

        botones.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        btn_newFile.setText("Nueva Carpeta");
        btn_newFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_newFileActionPerformed(evt);
            }
        });
        jPanel3.add(btn_newFile);

        btn_move.setText("Mover");
        btn_move.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_moveActionPerformed(evt);
            }
        });
        jPanel3.add(btn_move);

        btn_copy.setText("Copiar");
        btn_copy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_copyActionPerformed(evt);
            }
        });
        jPanel3.add(btn_copy);

        btn_erase.setText("Borrar");
        btn_erase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eraseActionPerformed(evt);
            }
        });
        jPanel3.add(btn_erase);

        botones.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        rutaText.setEditable(false);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rutaText, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rutaText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        botones.add(jPanel8, java.awt.BorderLayout.PAGE_END);

        jPanel2.add(botones, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setPreferredSize(new java.awt.Dimension(100, 17));
        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        nombreFile.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        nombreFile.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombreFile.setText("jLabel1");
        jPanel4.add(nombreFile);

        jPanel2.add(jPanel4, java.awt.BorderLayout.EAST);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setPreferredSize(new java.awt.Dimension(120, 0));

        btn_atras.setText("<");
        btn_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_atrasActionPerformed(evt);
            }
        });
        jPanel7.add(btn_atras);

        jPanel2.add(jPanel7, java.awt.BorderLayout.WEST);

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        panel_Files.setBackground(javax.swing.UIManager.getDefaults().getColor("window"));
        panel_Files.setLayout(new java.awt.BorderLayout());

        jPanel5.setLayout(new java.awt.BorderLayout());

        archivos_panel.setBackground(new java.awt.Color(255, 255, 255));
        archivos_panel.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("CheckBox.shadow")));
        archivos_panel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEADING));
        jScrollPane1.setViewportView(archivos_panel);

        jPanel5.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        AceptaCambios.setBackground(new java.awt.Color(255, 255, 255));
        AceptaCambios.setLayout(new javax.swing.OverlayLayout(AceptaCambios));

        move_panel.setLayout(new javax.swing.BoxLayout(move_panel, javax.swing.BoxLayout.LINE_AXIS));

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Archivo Seleccionado:");
        jPanel11.add(jLabel2);

        from.setText("jLabel3");
        jPanel11.add(from);

        move_panel.add(jPanel11);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        btn_action.setText("Mover");
        btn_action.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_actionActionPerformed(evt);
            }
        });
        jPanel12.add(btn_action);

        jButton4.setText("Cancelar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel12.add(jButton4);

        move_panel.add(jPanel12);

        AceptaCambios.add(move_panel);

        guardar_Panel.setLayout(new javax.swing.BoxLayout(guardar_Panel, javax.swing.BoxLayout.LINE_AXIS));

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(new java.awt.BorderLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Nombre");
        jPanel9.add(jLabel1, java.awt.BorderLayout.WEST);

        nombre.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        nombre.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        nombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nombreKeyPressed(evt);
            }
        });
        jPanel9.add(nombre, java.awt.BorderLayout.CENTER);

        guardar_Panel.add(jPanel9);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        btn_Guardar.setText("Guardar");
        btn_Guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_GuardarActionPerformed(evt);
            }
        });
        jPanel10.add(btn_Guardar);

        jButton3.setText("Cancelar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel10.add(jButton3);

        guardar_Panel.add(jPanel10);

        AceptaCambios.add(guardar_Panel);

        jPanel5.add(AceptaCambios, java.awt.BorderLayout.PAGE_END);

        panel_Files.add(jPanel5, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.disabledForeground")));
        jPanel6.setPreferredSize(new java.awt.Dimension(120, 0));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 17, 5));

        documentos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        documentos.setText("Documentos");
        documentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                documentosMouseClicked(evt);
            }
        });
        jPanel6.add(documentos);

        escritorio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        escritorio.setText("Escritorio");
        escritorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                escritorioMouseClicked(evt);
            }
        });
        jPanel6.add(escritorio);

        imagenes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        imagenes.setText("Imagenes");
        imagenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagenesMouseClicked(evt);
            }
        });
        jPanel6.add(imagenes);

        backup.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        backup.setText("     Backup     ");
        backup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backupMouseClicked(evt);
            }
        });
        jPanel6.add(backup);

        panel_Files.add(jPanel6, java.awt.BorderLayout.WEST);

        jPanel1.add(panel_Files, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void documentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_documentosMouseClicked
        
        FileControler.siguiente(FileLogin.routeDocument);
        
    }//GEN-LAST:event_documentosMouseClicked

    private void escritorioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_escritorioMouseClicked
        
        FileControler.siguiente(FileLogin.routeDesktop);
        
    }//GEN-LAST:event_escritorioMouseClicked

    private void imagenesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagenesMouseClicked
        FileControler.siguiente(FileLogin.routeImages);
    }//GEN-LAST:event_imagenesMouseClicked

    private void btn_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_atrasActionPerformed
        FileControler.atras();
    }//GEN-LAST:event_btn_atrasActionPerformed

    private void btn_newFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_newFileActionPerformed
        setVistaPanel(true,false,true);
        setBotones(true);
    }//GEN-LAST:event_btn_newFileActionPerformed

    private void btn_GuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_GuardarActionPerformed
        crearCarpeta();        
        this.guardar_Panel.setVisible(false);
    }//GEN-LAST:event_btn_GuardarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        setVistaPanel(false,false, false);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void nombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreKeyPressed
        if(evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){
            crearCarpeta();
        }
    }//GEN-LAST:event_nombreKeyPressed

    private void btn_eraseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eraseActionPerformed
        FileControler.deleteFile(nombreFile.getText());
    }//GEN-LAST:event_btn_eraseActionPerformed

    private void btn_moveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_moveActionPerformed
        setVistaPanel(true,true,false);
        
        setBotones(false);
        from.setText(nombreFile.getText());
        
        anterior = FileControler.rutaEspecifica+File.separator+nombreFile.getText();
        FileControler.moverMostrarFile(rutaText.getText());
    }//GEN-LAST:event_btn_moveActionPerformed

    private void btn_actionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_actionActionPerformed
        FileControler.moverTo(anterior, FileControler.rutaEspecifica, from.getText());
        
        if(btn_action.getText().equals("Mover"))
            FileControler.deleteFinal(new File(anterior));
        
        setVistaPanel(false,false,false);
        setBotones(true);
    }//GEN-LAST:event_btn_actionActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        setBotones(true);
        FileControler.mostrar(FileControler.rutaEspecifica);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btn_copyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_copyActionPerformed
        btn_action.setText("Copiar");
        setVistaPanel(true,true,false);
        
        setBotones(false);
        from.setText(nombreFile.getText());
        
        anterior = FileControler.rutaEspecifica+File.separator+nombreFile.getText();
        FileControler.moverMostrarFile(rutaText.getText());
    }//GEN-LAST:event_btn_copyActionPerformed

    private void backupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backupMouseClicked
        FileControler.siguiente(FileLogin.routeBackup);
    }//GEN-LAST:event_backupMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AceptaCambios;
    private javax.swing.JPanel archivos_panel;
    private javax.swing.JLabel backup;
    private javax.swing.JPanel botones;
    private javax.swing.JButton btn_Guardar;
    private javax.swing.JButton btn_action;
    private javax.swing.JButton btn_atras;
    private javax.swing.JButton btn_copy;
    private javax.swing.JButton btn_erase;
    private javax.swing.JButton btn_move;
    private javax.swing.JButton btn_newFile;
    private javax.swing.JLabel documentos;
    private javax.swing.JLabel escritorio;
    private javax.swing.JLabel from;
    private javax.swing.JPanel guardar_Panel;
    private javax.swing.JLabel imagenes;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel move_panel;
    private javax.swing.JTextField nombre;
    private javax.swing.JLabel nombreFile;
    private javax.swing.JPanel panel_Files;
    private javax.swing.JTextField rutaText;
    // End of variables declaration//GEN-END:variables
}
