package Escritorio;

import Escritorio.Controladores.FileControler;
import Login.FileLogin;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import javax.swing.ImageIcon;

public class CambiaFondo extends javax.swing.JInternalFrame {
    String fondoS;
    public static Image imagen=null;
    javax.swing.JDesktopPane esc;
            
    public CambiaFondo(javax.swing.JDesktopPane esc) {
        initComponents();
        
        this.esc=esc;
        this.setPreferredSize(new java.awt.Dimension(800, 535));
        
        FileControler.asignaPanel(this.imagenVista);
        FileControler.setFileControler(panelFiles, nombreFile, rutaText);
        FileControler.siguienteIMG(FileLogin.routeImages);
        
        this.setPreferredSize(new java.awt.Dimension(800, 535));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        back = new javax.swing.JButton();
        desk = new javax.swing.JButton();
        img = new javax.swing.JButton();
        doc = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rutaText = new javax.swing.JTextField();
        panelFiles = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        imagenVista = new javax.swing.JPanel(){
            @Override
            public void paint(Graphics g) {
                if (imagen != null) {
                    g.drawImage(imagen, 0, 0, getWidth(), getHeight(),this);
                    setOpaque(false);
                }else
                setOpaque(true);

                super.paint(g);
            }
        };
        files = new javax.swing.JPanel();
        nombreFile = new javax.swing.JLabel();
        select = new javax.swing.JButton();
        cancel = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(new java.awt.GridLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 0)));
        jPanel3.setPreferredSize(new java.awt.Dimension(389, 80));
        jPanel3.setLayout(new java.awt.GridLayout(0, 1));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        back.setText("Backup");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        jPanel4.add(back);

        desk.setText("Escritorio");
        desk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deskActionPerformed(evt);
            }
        });
        jPanel4.add(desk);

        img.setText("Imagenes");
        img.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imgActionPerformed(evt);
            }
        });
        jPanel4.add(img);

        doc.setText("Documentos");
        doc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                docActionPerformed(evt);
            }
        });
        jPanel4.add(doc);

        jPanel3.add(jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Ruta: ");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rutaText, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(rutaText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );

        jPanel3.add(jPanel5);

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_START);

        panelFiles.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(panelFiles, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 0), 1, true));
        jPanel2.setLayout(new java.awt.BorderLayout());

        imagenVista.setBackground(new java.awt.Color(255, 255, 255));
        imagenVista.setPreferredSize(new java.awt.Dimension(489, 296));

        javax.swing.GroupLayout imagenVistaLayout = new javax.swing.GroupLayout(imagenVista);
        imagenVista.setLayout(imagenVistaLayout);
        imagenVistaLayout.setHorizontalGroup(
            imagenVistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 487, Short.MAX_VALUE)
        );
        imagenVistaLayout.setVerticalGroup(
            imagenVistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.add(imagenVista, java.awt.BorderLayout.CENTER);

        files.setBackground(new java.awt.Color(255, 255, 255));
        files.add(nombreFile);

        select.setText("Seleccionar");
        select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectActionPerformed(evt);
            }
        });
        files.add(select);

        cancel.setText("Cancelar");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        files.add(cancel);

        jPanel2.add(files, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(jPanel2);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        FileControler.siguienteIMG(FileLogin.routeBackup);
    }//GEN-LAST:event_backActionPerformed

    private void deskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deskActionPerformed
        FileControler.siguienteIMG(FileLogin.routeDesktop);
    }//GEN-LAST:event_deskActionPerformed

    private void imgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imgActionPerformed
        FileControler.siguienteIMG(FileLogin.routeImages);
    }//GEN-LAST:event_imgActionPerformed

    private void docActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_docActionPerformed
        FileControler.siguienteIMG(FileLogin.routeDocument);
    }//GEN-LAST:event_docActionPerformed

    private void selectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectActionPerformed
        setImagen(rutaText.getText()+File.separator+nombreFile.getText());
    }//GEN-LAST:event_selectActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelActionPerformed

    public void setImagen(String nombreImagen) {
        if (nombreImagen != null) {
            Escritorio_Principal.imagen = new ImageIcon(nombreImagen).getImage();
        } else {
            CambiaFondo.imagen = null;
        }
 
        esc.repaint();
        esc.revalidate();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JButton cancel;
    private javax.swing.JButton desk;
    private javax.swing.JButton doc;
    private javax.swing.JPanel files;
    private javax.swing.JPanel imagenVista;
    private javax.swing.JButton img;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel nombreFile;
    private javax.swing.JPanel panelFiles;
    private javax.swing.JTextField rutaText;
    private javax.swing.JButton select;
    // End of variables declaration//GEN-END:variables
}
