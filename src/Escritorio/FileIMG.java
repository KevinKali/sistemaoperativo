
package Escritorio;

import Escritorio.Controladores.FileControler;
import Login.FileLogin;
import java.awt.Color;
import java.awt.Image;

import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class FileIMG extends javax.swing.JPanel {
    Controlador_IMG c;
    String tipo;
    javax.swing.JLabel nombreFile;
    javax.swing.JPanel panel;
    
    public FileIMG(String title, String imgR, javax.swing.JLabel nombreF,String tipo, javax.swing.JPanel p) {
        initComponents();
        c=new Controlador_IMG();
        
        this.tipo = tipo;
        nombreFile=nombreF;
        panel=p;
        
        this.title.setText(title);
        c.setIconoLable(img, imgR, 55, 55);
    }
    
    public void setImagen(String nombreImagen) {
        if (nombreImagen != null) {
            CambiaFondo.imagen = new ImageIcon(nombreImagen).getImage();
        } else {
            CambiaFondo.imagen = null;
        }
 
        panel.repaint();
        panel.revalidate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        cambiarNombre = new javax.swing.JMenuItem();
        panel1 = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        img = new javax.swing.JLabel();

        cambiarNombre.setText("Cambiar Nombre");
        cambiarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarNombreActionPerformed(evt);
            }
        });
        jPopupMenu1.add(cambiarNombre);

        setBackground(new java.awt.Color(204, 255, 204));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        panel1.setBackground(new java.awt.Color(204, 255, 204));
        panel1.setComponentPopupMenu(jPopupMenu1);
        panel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                panel1MouseClicked(evt);
            }
        });

        title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        title.setText("jLabel2");
        panel1.add(title);

        add(panel1, java.awt.BorderLayout.PAGE_END);

        img.setBackground(new java.awt.Color(255, 255, 255));
        img.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        img.setComponentPopupMenu(jPopupMenu1);
        img.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imgMouseClicked(evt);
            }
        });
        add(img, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void cambiarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarNombreActionPerformed
        
    }//GEN-LAST:event_cambiarNombreActionPerformed

    private void imgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imgMouseClicked
        nombreFile.setText(title.getText());
        
        if(tipo.equals("folder")){
            if (evt.getClickCount() >= 2) {
                FileControler.siguiente(FileControler.rutaEspecifica+File.separator+title.getText());
            }
        }else
            setImagen(FileLogin.routeImages+File.separator+title.getText());
    }//GEN-LAST:event_imgMouseClicked

    private void panel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel1MouseClicked
        nombreFile.setText(title.getText());
        if(tipo.equals("folder")){
            if (evt.getClickCount() >= 2) 
                FileControler.siguiente(FileControler.rutaEspecifica+File.separator+title.getText());
        }else
            setImagen(FileControler.rutaEspecifica+File.separator+title.getText());
        
    }//GEN-LAST:event_panel1MouseClicked

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        nombreFile.setText(title.getText());
        if(tipo.equals("folder")){
            if (evt.getClickCount() >= 2) 
                FileControler.siguiente(FileControler.rutaEspecifica+File.separator+title.getText());
        }else
            setImagen(FileControler.rutaEspecifica+File.separator+title.getText());
    }//GEN-LAST:event_formMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem cambiarNombre;
    private javax.swing.JLabel img;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPanel panel1;
    private javax.swing.JLabel title;
    // End of variables declaration//GEN-END:variables
}
