
package Escritorio;

import Apps.BlockNotas;
import Apps.Calculadora;
import Apps.HoraFecha;
import Apps.Juegos;
import Escritorio.Controladores.ControlAccesoSwing;
import Escritorio.Controladores.ControlaHiloTime;
import Navegador.Naveg;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyVetoException;

public class Escritorio_Principal extends javax.swing.JPanel {
    Controlador_IMG iconosC;
    Terminar terminarDialog;
    GestorFile fileView;
    TrashGestor trashView;
    String fondoS;
    public static Image imagen=null;
    
    private static final int widthIcon=32;
    private static final int heightIcon=32;
    public static BlockNotas block[] = new BlockNotas[10];
    public static Calculadora cal[] = new Calculadora[10];
            
    public Escritorio_Principal(java.awt.Frame escritorio) {
        initComponents();
        
        terminarDialog= new Terminar(escritorio,true);
        iconosC = new Controlador_IMG();
        
        setIcons();
        
        cargarEscritorio();
        (new ControlaHiloTime(time,date)).run();
        ControlAccesoSwing.hacerClick=true;
    }
    
    private void cargarEscritorio(){
        
    }
    
    public void setIcons(){
        iconosC.setIcono(btn_central, "/Images/Icons/central.png", widthIcon, heightIcon);
        iconosC.setIcono(btn_chrome, "/Images/Icons/navegador.jpg", widthIcon, heightIcon);
        iconosC.setIcono(btn_calc, "/Images/Icons/calcu1.jpg", widthIcon, heightIcon);
        iconosC.setIcono(btn_notepad, "/Images/Icons/notepad.jpg", widthIcon, heightIcon);
        iconosC.setIcono(btn_game, "/Images/Icons/juegos.jpg", widthIcon, heightIcon);
        iconosC.setIcono(btn_carpeta, "/Images/Icons/carpeta.jpg", widthIcon, heightIcon);
        
        iconosC.setIcono(btn_trash, "/Images/Icons/papeVacia.png", 40, 40);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rightClick = new javax.swing.JPopupMenu();
        fondo = new javax.swing.JMenuItem();
        aplicaciones = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btn_central = new javax.swing.JButton();
        btn_chrome = new javax.swing.JButton();
        btn_carpeta = new javax.swing.JButton();
        btn_notepad = new javax.swing.JButton();
        btn_game = new javax.swing.JButton();
        btn_calc = new javax.swing.JButton();
        btn_trash = new javax.swing.JButton();
        barraT = new javax.swing.JPanel();
        panelFecha = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        panelConf = new javax.swing.JPanel();
        panelApps = new javax.swing.JPanel();
        escritorio = new javax.swing.JDesktopPane(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(imagen, 0, 0, getWidth(), getHeight(), this);
            }
        };

        fondo.setText("Personalizar Fondo");
        fondo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fondoActionPerformed(evt);
            }
        });
        rightClick.add(fondo);

        setLayout(new java.awt.BorderLayout());

        aplicaciones.setBackground(java.awt.Color.lightGray);
        aplicaciones.setPreferredSize(new java.awt.Dimension(60, 300));
        aplicaciones.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.disabledShadow"));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 1));

        btn_central.setBackground(new java.awt.Color(255, 51, 51));
        btn_central.setText("asd");
        btn_central.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_centralActionPerformed(evt);
            }
        });
        jPanel1.add(btn_central);

        btn_chrome.setText("asd");
        btn_chrome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_chromeActionPerformed(evt);
            }
        });
        jPanel1.add(btn_chrome);

        btn_carpeta.setText("asd");
        btn_carpeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_carpetaActionPerformed(evt);
            }
        });
        jPanel1.add(btn_carpeta);

        btn_notepad.setText("asd");
        btn_notepad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_notepadActionPerformed(evt);
            }
        });
        jPanel1.add(btn_notepad);

        btn_game.setText("asd");
        btn_game.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gameActionPerformed(evt);
            }
        });
        jPanel1.add(btn_game);

        btn_calc.setText("asd");
        btn_calc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_calcActionPerformed(evt);
            }
        });
        jPanel1.add(btn_calc);

        aplicaciones.add(jPanel1, java.awt.BorderLayout.CENTER);

        btn_trash.setText("jButton1");
        btn_trash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_trashActionPerformed(evt);
            }
        });
        aplicaciones.add(btn_trash, java.awt.BorderLayout.SOUTH);

        add(aplicaciones, java.awt.BorderLayout.WEST);

        barraT.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));
        barraT.setPreferredSize(new java.awt.Dimension(0, 30));
        barraT.setLayout(new java.awt.BorderLayout());

        panelFecha.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.disabledShadow"));
        panelFecha.setPreferredSize(new java.awt.Dimension(418, 0));
        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout();
        flowLayout1.setAlignOnBaseline(true);
        panelFecha.setLayout(flowLayout1);

        date.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        date.setText("jLabel1");
        panelFecha.add(date);

        time.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        time.setText("jLabel1");
        panelFecha.add(time);

        barraT.add(panelFecha, java.awt.BorderLayout.CENTER);

        panelConf.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.disabledShadow"));
        panelConf.setPreferredSize(new java.awt.Dimension(418, 0));

        javax.swing.GroupLayout panelConfLayout = new javax.swing.GroupLayout(panelConf);
        panelConf.setLayout(panelConfLayout);
        panelConfLayout.setHorizontalGroup(
            panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
        );
        panelConfLayout.setVerticalGroup(
            panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        barraT.add(panelConf, java.awt.BorderLayout.EAST);

        panelApps.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.disabledShadow"));
        panelApps.setPreferredSize(new java.awt.Dimension(418, 0));

        javax.swing.GroupLayout panelAppsLayout = new javax.swing.GroupLayout(panelApps);
        panelApps.setLayout(panelAppsLayout);
        panelAppsLayout.setHorizontalGroup(
            panelAppsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
        );
        panelAppsLayout.setVerticalGroup(
            panelAppsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        barraT.add(panelApps, java.awt.BorderLayout.WEST);

        add(barraT, java.awt.BorderLayout.PAGE_START);

        escritorio.setBackground(new java.awt.Color(255, 255, 255));
        escritorio.setComponentPopupMenu(rightClick);
        escritorio.setPreferredSize(new java.awt.Dimension(100, 538));

        javax.swing.GroupLayout escritorioLayout = new javax.swing.GroupLayout(escritorio);
        escritorio.setLayout(escritorioLayout);
        escritorioLayout.setHorizontalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1195, Short.MAX_VALUE)
        );
        escritorioLayout.setVerticalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 608, Short.MAX_VALUE)
        );

        add(escritorio, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_centralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_centralActionPerformed
        terminarDialog.setVisible(true);
    }//GEN-LAST:event_btn_centralActionPerformed

    private void btn_carpetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_carpetaActionPerformed
        fileView = new GestorFile();
        this.escritorio.add(fileView);
        fileView.show();
    }//GEN-LAST:event_btn_carpetaActionPerformed

    private void fondoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fondoActionPerformed
        
        CambiaFondo f = new CambiaFondo(this.escritorio);
        escritorio.add(f);
        f.show();
        
    }//GEN-LAST:event_fondoActionPerformed

    private void btn_chromeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_chromeActionPerformed
        NativeInterface.open();
        Naveg n=new Naveg();
        escritorio.add(n);
        n.show();
    }//GEN-LAST:event_btn_chromeActionPerformed

    private void btn_trashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_trashActionPerformed
        trashView = new TrashGestor();
        escritorio.add(trashView);
        trashView.show();
        
    }//GEN-LAST:event_btn_trashActionPerformed

    private void btn_notepadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_notepadActionPerformed
        int i;
        for(i=0;i<10;i++)
            if(block[i]==null)
                break;
        if(i<10)
        {
            for(int j=0;j<10;j++){
                if(cal[j]!=null){
                    
                    try {
                        cal[j].setIcon(true);
                    } catch (PropertyVetoException ex) {}
                }
                if(block[j]!=null){
                    try {
                            block[j].setIcon(true);
                    } catch (PropertyVetoException ex) {}
                }
            }
            block[i] = new BlockNotas(i);
            escritorio.add(block[i]);
            Dimension desktopSize = escritorio.getSize();
            Dimension jInternalFrameSize = block[i].getSize();
            block[i].setLocation((desktopSize.width - jInternalFrameSize.width)/2,(desktopSize.height- jInternalFrameSize.height)/2);
            block[i].show();
        }
    }//GEN-LAST:event_btn_notepadActionPerformed

    private void btn_gameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gameActionPerformed
        Juegos jugar=new Juegos();
        escritorio.add(jugar);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = jugar.getSize();
        jugar.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        jugar.show();
    }//GEN-LAST:event_btn_gameActionPerformed

    private void btn_calcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_calcActionPerformed
        Calculadora calcu=new Calculadora();
        escritorio.add(calcu);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = calcu.getSize();
        calcu.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        calcu.show();
    }//GEN-LAST:event_btn_calcActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel aplicaciones;
    private javax.swing.JPanel barraT;
    private javax.swing.JButton btn_calc;
    private javax.swing.JButton btn_carpeta;
    private javax.swing.JButton btn_central;
    private javax.swing.JButton btn_chrome;
    private javax.swing.JButton btn_game;
    private javax.swing.JButton btn_notepad;
    private javax.swing.JButton btn_trash;
    private javax.swing.JLabel date;
    public static javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenuItem fondo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panelApps;
    private javax.swing.JPanel panelConf;
    private javax.swing.JPanel panelFecha;
    private javax.swing.JPopupMenu rightClick;
    private javax.swing.JLabel time;
    // End of variables declaration//GEN-END:variables
}
