
package Escritorio;

import Apps.BlockNotas;
import Escritorio.Controladores.ControlAccesoSwing;
import Escritorio.Controladores.FileControler;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Carpeta extends javax.swing.JPanel {
    Controlador_IMG c;
    String tipo;
    javax.swing.JLabel nombreFile;
    ImageView frameI;
    
    public Carpeta(String title, String imgR, javax.swing.JLabel nombreF,String tipo) {
        initComponents();
        c=new Controlador_IMG();
        
        this.tipo = tipo;
        nombreFile=nombreF;
        this.title.setText(title);
        c.setIconoLable(img, imgR, 55, 55);
        
        panel2.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        cambiarNombre = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        panel2 = new javax.swing.JPanel();
        newName = new javax.swing.JTextField();
        panel1 = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        img = new javax.swing.JLabel();

        cambiarNombre.setText("Cambiar Nombre");
        cambiarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarNombreActionPerformed(evt);
            }
        });
        jPopupMenu1.add(cambiarNombre);

        setBackground(new java.awt.Color(51, 255, 51));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        jPanel3.setLayout(new javax.swing.OverlayLayout(jPanel3));

        panel2.setLayout(new javax.swing.BoxLayout(panel2, javax.swing.BoxLayout.LINE_AXIS));

        newName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                newNameFocusLost(evt);
            }
        });
        newName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                newNameKeyPressed(evt);
            }
        });
        panel2.add(newName);

        jPanel3.add(panel2);

        panel1.setComponentPopupMenu(jPopupMenu1);

        title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        title.setText("jLabel2");
        title.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                seleccionada(evt);
            }
        });
        panel1.add(title);

        jPanel3.add(panel1);

        add(jPanel3, java.awt.BorderLayout.PAGE_END);

        img.setComponentPopupMenu(jPopupMenu1);
        img.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                seleccionada(evt);
            }
        });
        add(img, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void seleccionada(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_seleccionada
        if(tipo.equals("folder")){
            if (evt.getClickCount() >= 2) {
                FileControler.siguiente(FileControler.rutaEspecifica+File.separator+title.getText());
            }
        }else{
            if (evt.getClickCount() >= 2) {
                frameI = new ImageView();
                frameI.setData(title.getText(), FileControler.rutaEspecifica);
                Escritorio_Principal.escritorio.add(frameI);
                frameI.show();
            }
        }
        nombreFile.setText(title.getText());
    }//GEN-LAST:event_seleccionada

    private void cambiarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarNombreActionPerformed
        String anterior = title.getText();
        
        panel1.setVisible(false);
        panel2.setVisible(true);
        newName.requestFocus();
    }//GEN-LAST:event_cambiarNombreActionPerformed

    private void newNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_newNameKeyPressed
        if(evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){
            if(!newName.getText().equals("")){
                String n = FileControler.rutaEspecifica+File.separator+title.getText();
                
                title.setText(newName.getText()+FileControler.getTipoFile(n));
                
                FileControler.moverTo(n, FileControler.rutaEspecifica, newName.getText()+"."+FileControler.getTipoFile(n));
                FileControler.deleteFinal(new File(n));
                FileControler.mostrar(FileControler.rutaEspecifica);
                
                newName.requestFocus(false);
                panel1.setVisible(true);
                panel2.setVisible(false);
            }
        }else if(evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){
            panel1.setVisible(true);
            panel2.setVisible(false);
        }
    }//GEN-LAST:event_newNameKeyPressed

    private void newNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_newNameFocusLost
        panel1.setVisible(true);
        panel2.setVisible(false);
    }//GEN-LAST:event_newNameFocusLost

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        if(tipo.equals("folder")){
            if (evt.getClickCount() >= 2) 
                FileControler.siguiente(FileControler.rutaEspecifica+File.separator+title.getText());
        }else if(tipo.equals("image")){
            if (evt.getClickCount() >= 2) {
                frameI = new ImageView();
                frameI.setData(title.getText(), FileControler.rutaEspecifica);
                Escritorio_Principal.escritorio.add(frameI);
                frameI.show();
            }
        }
        nombreFile.setText(title.getText());
    }//GEN-LAST:event_formMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem cambiarNombre;
    private javax.swing.JLabel img;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JTextField newName;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JLabel title;
    // End of variables declaration//GEN-END:variables
}
