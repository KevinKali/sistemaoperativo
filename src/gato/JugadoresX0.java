
package gato;

import static Escritorio.Escritorio_Principal.escritorio;
import Apps.Juegos;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class JugadoresX0 extends javax.swing.JInternalFrame 
{
    AudioClip audio, audio1, inicio;
    
    public JugadoresX0() 
    {
        initComponents();
        setResizable(false);
        setSize(626, 250);
    }
    
    void pedirNombre() {

        if (j1.getText().equals("") || j2.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "¡Ingresar ambos\n    jugadores!", "Gato", 0,
                    new javax.swing.ImageIcon(getClass().getResource("/gato/aj.png")));
            inicio = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
            inicio.play();
        } else {
            if (j1.getText().equalsIgnoreCase(j2.getText())) {
                JOptionPane.showMessageDialog(null, "¡Asignar nombres diferentes\n  para los jugadores!", "Gato", 0,
                        new javax.swing.ImageIcon(getClass().getResource("/gato/aj.png")));
                inicio = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
                inicio.play();
                j1.setText("");
                j2.setText("");
            } else {
                TableroJugador t = new TableroJugador();
                escritorio.add(t);
                dispose();
                t.tit.setText("Tú turno " + j1.getText());
                t.tit.setForeground(Color.yellow);
                t.jug1.setText(j1.getText());
                t.jug2.setText(j2.getText());  
                Dimension desktopSize = escritorio.getSize();
                Dimension FrameSize = t.getSize();
                t.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
                t.show();
                JOptionPane.showMessageDialog(null, "¡ Que gane el mejor !","Gato",JOptionPane.INFORMATION_MESSAGE);
                
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelNice1 = new org.edisoncor.gui.panel.PanelNice();
        panelCurves1 = new org.edisoncor.gui.panel.PanelCurves();
        pnlResultado1 = new javax.swing.JPanel();
        juga1 = new javax.swing.JLabel();
        j1 = new javax.swing.JTextField();
        pnlResultado2 = new javax.swing.JPanel();
        juga2 = new javax.swing.JLabel();
        j2 = new javax.swing.JTextField();
        cancel = new javax.swing.JButton();
        acep = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setPreferredSize(new java.awt.Dimension(624, 267));

        pnlResultado1.setBackground(new java.awt.Color(255, 255, 0));
        pnlResultado1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlResultado1.setForeground(new java.awt.Color(51, 51, 51));

        juga1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        juga1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        juga1.setText("Jugador 1");

        j1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        j1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        j1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        j1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j1ActionPerformed(evt);
            }
        });
        j1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                j1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                j1KeyTyped(evt);
            }
        });

        javax.swing.GroupLayout pnlResultado1Layout = new javax.swing.GroupLayout(pnlResultado1);
        pnlResultado1.setLayout(pnlResultado1Layout);
        pnlResultado1Layout.setHorizontalGroup(
            pnlResultado1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultado1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(juga1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
            .addGroup(pnlResultado1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlResultado1Layout.setVerticalGroup(
            pnlResultado1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultado1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(juga1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlResultado2.setBackground(new java.awt.Color(51, 204, 0));
        pnlResultado2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        juga2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        juga2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        juga2.setText("Jugador 2");

        j2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        j2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        j2.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        j2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                j2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                j2KeyTyped(evt);
            }
        });

        javax.swing.GroupLayout pnlResultado2Layout = new javax.swing.GroupLayout(pnlResultado2);
        pnlResultado2.setLayout(pnlResultado2Layout);
        pnlResultado2Layout.setHorizontalGroup(
            pnlResultado2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultado2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(juga2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47))
            .addGroup(pnlResultado2Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlResultado2Layout.setVerticalGroup(
            pnlResultado2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlResultado2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(juga2)
                .addGap(18, 18, 18)
                .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cancel.setText("CANCELAR");
        cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });
        cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cancelKeyPressed(evt);
            }
        });

        acep.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        acep.setText("ACEPTAR");
        acep.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        acep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acepActionPerformed(evt);
            }
        });
        acep.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                acepKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("¡¡¡ INGRESAR JUGADORES !!!");

        javax.swing.GroupLayout panelCurves1Layout = new javax.swing.GroupLayout(panelCurves1);
        panelCurves1.setLayout(panelCurves1Layout);
        panelCurves1Layout.setHorizontalGroup(
            panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCurves1Layout.createSequentialGroup()
                .addGroup(panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCurves1Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(acep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlResultado1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlResultado2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelCurves1Layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(jLabel1)))
                .addContainerGap(106, Short.MAX_VALUE))
        );
        panelCurves1Layout.setVerticalGroup(
            panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCurves1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlResultado1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlResultado2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCurves1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(acep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        panelNice1.add(panelCurves1, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelNice1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelNice1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void j1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j1ActionPerformed
        j2.requestFocus();
    }//GEN-LAST:event_j1ActionPerformed

    private void j1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_j1KeyReleased
        j1.setText(j1.getText().toUpperCase());
    }//GEN-LAST:event_j1KeyReleased

    private void j1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_j1KeyTyped
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && (car != (char) KeyEvent.VK_ENTER) && (car != (char) KeyEvent.VK_BACK_SPACE)) {
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
        }
        if (j1.getText().length() == 8) {
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null, "   ¡Alcanzo el limite\npermitido de letras!", "Mensaje", 0,
                new javax.swing.ImageIcon(getClass().getResource("/gato/letra.png")));
        }
    }//GEN-LAST:event_j1KeyTyped

    private void j2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_j2KeyReleased
        j2.setText(j2.getText().toUpperCase());
    }//GEN-LAST:event_j2KeyReleased

    private void j2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_j2KeyTyped
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && (car != (char) KeyEvent.VK_ENTER) && (car != (char) KeyEvent.VK_BACK_SPACE)) {
            evt.consume();
        }
        if (j1.getText().length() == 8) {
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null, "   ¡Alcanzo el limite\npermitido de letras!", "Mensaje", 0,
                new javax.swing.ImageIcon(getClass().getResource("/gato/letra.png")));
        }
    }//GEN-LAST:event_j2KeyTyped

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        inicio = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
        inicio.play();
        dispose();
        Juegos jugar=new Juegos();
        escritorio.add(jugar);
        Dimension desktopSize = escritorio.getSize();
        Dimension FrameSize = jugar.getSize();
        jugar.setLocation((desktopSize.width - FrameSize.width)/2, (desktopSize.height- FrameSize.height)/2);
        jugar.show();
        //new Ventana().setVisible(true);
    }//GEN-LAST:event_cancelActionPerformed

    private void cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cancelKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            audio1 = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
            audio1.play();
            dispose();
            Juegos jugar=new Juegos();
            jugar.show();
            //new Ventana().setVisible(true);
        }
    }//GEN-LAST:event_cancelKeyPressed

    private void acepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acepActionPerformed
        inicio = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
        inicio.play();
        pedirNombre();
    }//GEN-LAST:event_acepActionPerformed

    private void acepKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_acepKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            audio1 = java.applet.Applet.newAudioClip(getClass().getResource("/gato/2.mp3"));
            audio1.play();
            pedirNombre();
        }
    }//GEN-LAST:event_acepKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton acep;
    private javax.swing.JButton cancel;
    private javax.swing.JTextField j1;
    private javax.swing.JTextField j2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel juga1;
    private javax.swing.JLabel juga2;
    private org.edisoncor.gui.panel.PanelCurves panelCurves1;
    private org.edisoncor.gui.panel.PanelNice panelNice1;
    private javax.swing.JPanel pnlResultado1;
    private javax.swing.JPanel pnlResultado2;
    // End of variables declaration//GEN-END:variables
}
