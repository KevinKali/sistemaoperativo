
package Login;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

public class FileLogin {
    public static final String routePrincipal = "C:"+File.separator+"practicaSO";
    public static final String routeDocument = "C:"+File.separator+"practicaSO"+File.separator+"Documentos";
    public static final String routeDesktop = "C:"+File.separator+"practicaSO"+File.separator+"Escritorio";
    public static final String routeImages = "C:"+File.separator+"practicaSO"+File.separator+"Images";
    public static final String routeTrash = "C:"+File.separator+"practicaSO"+File.separator+"Papelera";
    public static final String routeBackup = "C:"+File.separator+"practicaSO"+File.separator+"Backup";
    
    static final String ruta_txt = routePrincipal+File.separator+"login.txt";
    File ruta;
    String file_destino;
    
    public boolean escribir(String userName, String password){
        
        ruta = new File(routePrincipal);
        ruta.mkdir();
        
        (new File(routeDesktop)).mkdir();
        (new File(routeDocument)).mkdir();
        (new File(routeImages)).mkdir();
        (new File(routeTrash)).mkdir();
        (new File(routeBackup)).mkdir();
        
        ruta = new File(ruta_txt);
        file_destino = ruta.getAbsolutePath();
        
        try {
            ruta.createNewFile();
            
            return guardaDatos(userName, password);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error al Guardar datos del usuario");
            return false;
        }
    }
    
    private boolean guardaDatos(String userName, String password){
        FileWriter escritura;
        try {
            escritura = new FileWriter(file_destino,false);
            
            escritura.write(userName);
            escritura.write("\r\n");
            escritura.write(password);
            
            escritura.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
    
    public String[] leer(){
        String datos[] = new String[2];
        
        try{
            FileReader ruta = new FileReader(ruta_txt);
            BufferedReader leer = new BufferedReader(ruta);
            
            int i=0;
            String linea="";
            
            while(linea!=null){
                linea = leer.readLine();
                
                if(linea!=null){
                    datos[i++]=linea;
                }
            }
            leer.close();
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Error al leer datos de usuario");
        }
        
        return datos;
    }
    
    public static boolean entrada(){
        File ruta = new File(ruta_txt);
        if(ruta.exists())
            return true;
        else
            return false;
    }
}
