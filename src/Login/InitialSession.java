
package Login;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

public class InitialSession extends javax.swing.JPanel {
    private FileLogin l = new FileLogin();
    private JDesktopPane pantalla;
    private MainL m;
    
    public InitialSession(JDesktopPane pantalla, MainL m) {
        initComponents();
        limpia_Pantalla(true,false);
        
        this.pantalla=pantalla;
        this.m=m;
    }
    
    private void limpia_Pantalla(boolean p1, boolean p2){
        form1.setVisible(p1);
        form2.setVisible(p2);
        if(p1==true)
            txt_user.requestFocus();
        else
            txt_password1.requestFocus();
    }
    
    private void limpiar_contras(){
        txt_password1.setText("");
        txt_password2.setText("");
    }
    
    private void acceder(){
        char contra1[] = txt_password1.getPassword();
        String passwd1 = new String(contra1);
        
        char contra2[] = txt_password2.getPassword();
        String passwd2 = new String(contra2);
        
        if(passwd1.equals(passwd2)){
            if(l.escribir(txt_user.getText(), passwd1)){
                pantalla.remove(this);
                pantalla.add(m);
                pantalla.repaint();
                pantalla.revalidate();
            }else{
                limpia_Pantalla(true,false);
            }
        }else{
            txt_user.setText("");
            txt_password1.requestFocus();
            limpiar_contras();
            JOptionPane.showMessageDialog(null, "Contraseñas incorrectas");
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        container_panel = new javax.swing.JPanel();
        form2 = new javax.swing.JPanel();
        lb_content2 = new javax.swing.JLabel();
        lb_aviso2 = new javax.swing.JLabel();
        adelante = new javax.swing.JButton();
        txt_password2 = new javax.swing.JPasswordField();
        txt_password1 = new javax.swing.JPasswordField();
        lb_content3 = new javax.swing.JLabel();
        atras = new javax.swing.JButton();
        form1 = new javax.swing.JPanel();
        lb_content1 = new javax.swing.JLabel();
        lb_aviso1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        txt_user = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(102, 153, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 36)); // NOI18N
        jLabel1.setText("kaliTibu");

        jLabel2.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jLabel2.setText("Bienvenido usuario root");

        container_panel.setLayout(new javax.swing.OverlayLayout(container_panel));

        form2.setBackground(new java.awt.Color(204, 204, 255));

        lb_content2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lb_content2.setText("Confirm Password");

        lb_aviso2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lb_aviso2.setText("Porfavor Ingresar contraseña de usuario root");

        adelante.setText(">");
        adelante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adelanteActionPerformed(evt);
            }
        });

        txt_password2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        txt_password2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_password2KeyPressed(evt);
            }
        });

        txt_password1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        txt_password1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_password1KeyPressed(evt);
            }
        });

        lb_content3.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lb_content3.setText("Password");

        atras.setText("<");
        atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout form2Layout = new javax.swing.GroupLayout(form2);
        form2.setLayout(form2Layout);
        form2Layout.setHorizontalGroup(
            form2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(form2Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(form2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lb_aviso2)
                    .addComponent(txt_password2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_content2)
                    .addComponent(txt_password1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_content3))
                .addContainerGap(24, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, form2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(atras)
                .addGap(18, 18, 18)
                .addComponent(adelante)
                .addGap(119, 119, 119))
        );
        form2Layout.setVerticalGroup(
            form2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(form2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(lb_content3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_password1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lb_content2)
                .addGap(1, 1, 1)
                .addComponent(txt_password2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lb_aviso2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(form2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adelante)
                    .addComponent(atras))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        container_panel.add(form2);

        form1.setBackground(new java.awt.Color(204, 204, 255));

        lb_content1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lb_content1.setText("UserName");

        lb_aviso1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lb_aviso1.setText("Porfavor Ingresar nombre de usuario root");

        jButton2.setText(">");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        txt_user.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        txt_user.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_userKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout form1Layout = new javax.swing.GroupLayout(form1);
        form1.setLayout(form1Layout);
        form1Layout.setHorizontalGroup(
            form1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(form1Layout.createSequentialGroup()
                .addGroup(form1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(form1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(lb_content1))
                    .addGroup(form1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(txt_user, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(form1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(lb_aviso1))
                    .addGroup(form1Layout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addComponent(jButton2)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        form1Layout.setVerticalGroup(
            form1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(form1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(lb_content1)
                .addGap(6, 6, 6)
                .addComponent(txt_user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lb_aviso1)
                .addGap(11, 11, 11)
                .addComponent(jButton2)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        container_panel.add(form1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(container_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(225, 225, 225))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(122, 122, 122))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(47, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(29, 29, 29)
                .addComponent(container_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jButton1.setBackground(new java.awt.Color(0, 102, 255));
        jButton1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Apagar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(330, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(330, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(39, 39, 39))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(134, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap(43, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(!txt_user.getText().equals("")){
            limpia_Pantalla(false,true);
        }else
            Toolkit.getDefaultToolkit().beep();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void adelanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adelanteActionPerformed
        acceder();
    }//GEN-LAST:event_adelanteActionPerformed

    private void txt_password1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_password1KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txt_password2.requestFocus();
    }//GEN-LAST:event_txt_password1KeyPressed

    private void atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasActionPerformed
        limpiar_contras();
        limpia_Pantalla(true,false);
    }//GEN-LAST:event_atrasActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_password2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_password2KeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            acceder();
        
    }//GEN-LAST:event_txt_password2KeyPressed

    private void txt_userKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_userKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            limpia_Pantalla(false,true);
    }//GEN-LAST:event_txt_userKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adelante;
    private javax.swing.JButton atras;
    private javax.swing.JPanel container_panel;
    private javax.swing.JPanel form1;
    private javax.swing.JPanel form2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lb_aviso1;
    private javax.swing.JLabel lb_aviso2;
    private javax.swing.JLabel lb_content1;
    private javax.swing.JLabel lb_content2;
    private javax.swing.JLabel lb_content3;
    private javax.swing.JPasswordField txt_password1;
    private javax.swing.JPasswordField txt_password2;
    private javax.swing.JTextField txt_user;
    // End of variables declaration//GEN-END:variables
}
